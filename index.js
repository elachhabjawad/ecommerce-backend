const jsonServer = require("json-server");
const auth = require("json-server-auth");
const server = jsonServer.create();
const router = jsonServer.router("db.json");
const middlewares = jsonServer.defaults();
const port = process.env.PORT || 8080; 

server.db = router.db; // Set db for json-server-auth to use
server.use(middlewares);
server.use(auth); // Use json-server-auth
server.use(router);

server.listen(port);
